import Boom from 'boom'

const validate = (decoded, req, cb) => {
  req.db.manager.fetch('user', { id: decoded.id }).then(model => {
    if (model === null) {
      return cb(null, false)
    }
    cb(null, true)
  })
  .catch(err => {
    console.error(err)
    return cb(null, false)
  })
}

export default (server, key) => {
  server.auth.strategy(
    'jwt',
    'jwt',
    {
      key,
      validateFunc: validate,
      verifyOptions: {
        algorithms: [ 'HS256' ]
      }
    })

    server.auth.default('jwt');
}
