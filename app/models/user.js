export default {
  tableName: 'users',
  bcrypt: {
    field: 'password'
  },
  hasTimestamps: ['created_at', 'updated_at']
}
