export default {
  tableName: 'records',
  hasTimestamps: ['created_at', 'updated_at']
}
