import { resource } from 'nihapi/lib/router_helpers'
import RecordsResource from './resources/records'
import RecordsValidator from './validators/records'

export default resource('records', RecordsResource, RecordsValidator, {
  index: {
    config: {
      description: 'Returns list of records',
      tags: ['resource'],
      auth: 'jwt'
    }
  },
  show: {
    config: {
      description: 'Returns one record',
      tags: ['resource'],
      auth: 'jwt'
    }
  },
  create: {
    config: {
      description: 'Create a record',
      tags: ['resource'],
      auth: 'jwt'
    }
  },
  update: {
    config: {
      description: 'Update the record',
      tags: ['resource'],
      auth: 'jwt'
    }
  },
  destroy: {
    config: {
      description: 'Destroy the record',
      tags: ['resource'],
      auth: 'jwt'
    }
  },
  validate: {
    config: {
      description: 'Validate the record',
      tags: ['resource'],
      auth: 'jwt'
    }
  }
})
