import Joi from 'joi'

export default {
  index: {},
  show: {},
  create: {
    payload: {
      type: Joi.string().max(200).required(),
      content: Joi.object().required(),
    }
  },
  update: {
    payload: {
      type: Joi.string().max(200),
      content: Joi.object()
    }
  },
  delete: {},
  validate: {
    payload: {
      type: Joi.string().max(200).required(),
      content: Joi.object().required()
    }
  }
}
