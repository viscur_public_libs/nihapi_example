import BaseResource from 'nihapi/lib/base_resource'

export default class RecordsResource extends BaseResource {
  name = 'record'

  readAttrs = [
    'id',
    'type',
    'content',
    'created_at',
    'updated_at'
  ]
}
