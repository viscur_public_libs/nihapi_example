import { group } from 'nihapi/lib/router_helpers'
import records from './records/routes'
import auth from './auth/routes'

export default group([records, auth], {
  prefix: '/api/v1'
})
