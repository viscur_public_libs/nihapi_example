import { resource } from 'nihapi/lib/router_helpers'
import AuthResource from './resources/auth'
import AuthValidator from './validators/auth'

export default resource('auth', AuthResource, AuthValidator, {
  actions: ['login', 'register'],
  login: {
    method: 'POST',
    path: '/auth/login',
    config: {
      handler(req, reply) {
        new AuthResource(req, reply).login()
      },
      validate: AuthValidator.login,
      description: 'Login a user',
      tags: ['api'],
      auth: false
    }
  },
  register: {
    method: 'POST',
    path: '/auth/register',
    config: {
      handler(req, reply) {
        new AuthResource(req, reply).register()
      },
      validate: AuthValidator.register,
      description: 'Register a user',
      tags: ['api'],
      auth: false
    }
  },
  me: {
    method: 'GET',
    path: '/auth/me',
    config: {
      handler(req, reply) {
        new AuthResource(req, reply).me()
      },
      validate: AuthValidator.me,
      description: 'Return user profile',
      tags: ['api'],
      auth: 'jwt'
    }
  }
})
