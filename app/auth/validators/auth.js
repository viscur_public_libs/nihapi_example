import Joi from 'joi'

export default {
  login: {
    payload: {
      email: Joi.string().email().required(),
      password: Joi.string().max(30).required()
    }
  },
  register: {
    payload: {
      email: Joi.string().email().required(),
      password: Joi.string().max(30).required()
    }
  }
}
