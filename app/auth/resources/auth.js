import Boom from 'boom'
import JWT from 'jsonwebtoken'
import { omit } from 'lodash'
import BaseResource from 'nihapi/lib/base_resource'
import config from '../../../config'

export default class AuthResource extends BaseResource {
  name = 'user'

  register() {
    this.manager.forge(this.name)
      .where('email', this.body.email).fetch().then(user => {
        if (user === null) {
          return this.manager.create(this.name, this.body).then(() => {
            this.reply({}).code(201)
          })
        }
        return this.reply(Boom.badRequest('User already exist'))
      })
  }

  login() {
    this.manager.forge(this.name).where('email', this.body.email).fetch().then(user => {
      if (user === null) { return this.reply(Boom.notFound()) }
      const valid = user.compare(this.body.password)
      if (!valid) { return this.reply(Boom.unathorized('Invalid password')) }
      const token = JWT.sign(user.serialize(), config.jwtKey)
      this.reply({ token })
    })
  }

  me() {
    this.reply({
      data: omit(this.req.auth.credentials,
                 ['token', 'iat'])
    })
  }
}
