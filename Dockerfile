FROM node:7.5.0

RUN apt-get update && apt-get install -y build-essential

RUN npm i -g yarn nodemon knex

RUN mkdir /opt/api

WORKDIR /opt/api

COPY . /opt/api
