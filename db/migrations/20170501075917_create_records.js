module.exports = {
  up(knex) {
    return knex.schema.createTable('records', table => {
      table.increments()

      table.string('type')
      table.json('content')
      table.integer('user_id').unsigned().index()

      table.timestamps()
    })
  },

  down(knex) {
    return knex.schema.dropTable('records')
  }
}
