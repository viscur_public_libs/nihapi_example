module.exports = {
  up(knex) {
    return knex.schema.createTable('users', table => {
      table.increments()

      table.string('email').notNullable()
      table.string('password')

      table.timestamps()
    })
  },

  down(knex) {
    return knex.schema.dropTable('users')
  }
}
