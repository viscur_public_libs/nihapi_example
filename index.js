import { Server } from 'hapi'
import config from './config'
import initializers from './initializers'
import routes from './app/routes'
import auth from './auth'

const server = new Server()

server.connection({
  port: config.port,
  router: {
    stripTrailingSlash: true
  }
})

server.register(initializers, err => {
  if (err) {
    console.error('Failed to load initializers')
    console.error(err)
    process.exit(1)
  }

  auth(server, config.jwtKey)

  server.route(routes)

  server.start(err => {
    if (err) {
      console.error(err)
      process.exit(1)
    }
    console.log(`Server running at: ${server.info.uri}`)
  })
})

