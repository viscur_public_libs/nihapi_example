export default {
  port: process.env.NODE_PORT,
  jwtKey: process.env.JWT_KEY
}
