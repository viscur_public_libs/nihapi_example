import knex from 'knex'
import bookshelf from 'bookshelf'
import bookshelfManager from 'bookshelf-manager'
import bookshelfBcrypt from 'bookshelf-bcrypt'
import modelsLoader from 'nihapi/lib/models_loader'
import dbConf from '../knexfile'

const env = process.env.NODE_ENV || 'development'
const dbConfig = dbConf[env]
const db = knex(dbConfig)

if (process.env.NODE_ENV === 'production') {
  db.migrate.latest()
}

const bs = bookshelf(knex(dbConfig))

bs.plugin('pagination')
bs.plugin('registry')
bs.plugin(bookshelfManager)
bs.plugin(bookshelfBcrypt)

modelsLoader(bs, __dirname)

const hapiBookshelf = {
  register(server, options, next) {
    server.decorate('request', 'db', bs)
    next()
  }
}

hapiBookshelf.register.attributes = {
  name: 'myPlugin',
  version: '1.0.0'
}

export default hapiBookshelf
