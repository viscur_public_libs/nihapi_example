import Good from 'good'

export default {
  register: Good,
  options: {
    reporters: {
      console: [{
        module: 'good-squeeze',
        name: 'Squeeze',
        args: [{
          response: '*',
          logs: '*'
        }]
      }, {
        module: 'good-console'
      }, 'stdout'],
    }
  }
}
