import Swagger from 'hapi-swagger'
import pack from '../package.json'

export default {
  register: Swagger,
  options: {
    basePath: '/api/v1/',
    info: {
      title: 'Test title',
      version: pack.version
    },
    tags: [{
      name: 'api',
      description: 'Docs for api'
    }],
    securityDefinitions: {
      jwt: {
        type: 'apiKey',
        name: 'Authorization',
        in: 'header'
      }
    }
  }
}
