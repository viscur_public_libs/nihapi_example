import Vision from './vision'
import Inert from './inert'
import Swagger from './swagger'
import Good from './good'
import Qs from './qs'
import Db from './db'
import Auth from './auth'

export default [Inert, Vision, Swagger, Good, Qs, Db, Auth]
